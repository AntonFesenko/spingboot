package com.example.exception.handler;

import com.example.exception.ApplicationGlobalException;
import com.example.exception.ManufacturerValidationException;
import com.example.exception.VehicleValidationException;
import com.example.exception.model.ErrorMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ManufacturerValidationException.class})
    public ResponseEntity<ErrorMessage> handleExceptionManufacturer(ApplicationGlobalException exception, HttpServletRequest request) {
        var httpStatus = exception.getHttpStatus();

        loggingExceptionPath(exception, request);

        ErrorMessage errorMessage = fullErrorMessage(exception, httpStatus, request);

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {VehicleValidationException.class})
    public ResponseEntity<ErrorMessage> handleExceptionVehicle(ApplicationGlobalException exception, HttpServletRequest request) {
        var httpStatus = exception.getHttpStatus();

        loggingExceptionPath(exception, request);

        ErrorMessage errorMessage = fullErrorMessage(exception, httpStatus, request);

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    private void loggingExceptionPath(ApplicationGlobalException exception, HttpServletRequest request) {
        log.error(String.format("Exception received, path: '%s'", request.getRequestURI()), exception);
    }

    private ErrorMessage fullErrorMessage(ApplicationGlobalException exception, HttpStatus httpStatus, HttpServletRequest request) {
        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(exception.getMessage())
                .statusCode(httpStatus.value())
                .timestamp(System.currentTimeMillis())
                .pathError(String.format("Exception received, path: '%s'", request.getRequestURI()))
                .error(exception.getClass().getName()).build();
        return errorMessage;
    }

}
