package com.example.data.repository;

import com.example.data.entity.Manufacturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ManufacturerRepository extends CrudRepository<Manufacturer, Long> {

    Optional<Manufacturer> findManufacturerByCompanyName(String companyName);
}
