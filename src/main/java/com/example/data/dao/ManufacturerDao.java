package com.example.data.dao;


import com.example.data.entity.Manufacturer;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ManufacturerDao {

    Manufacturer findById(Long id);

    Manufacturer save(Manufacturer manufacturer);

    List<Manufacturer> findAll();

    Manufacturer findIdByCompanyName(String companyName);

}
