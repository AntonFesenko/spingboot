package com.example.data.dao;

import com.example.data.entity.Vehicle;

import java.util.List;

public interface VehicleDao {

    List findAll();

    Vehicle save(Vehicle vehicle);

    void delete(Long id);

    Vehicle findById(Long id);

    Vehicle update(Vehicle vehicle);
}

