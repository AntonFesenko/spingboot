package com.example.data.dao.impl;

import com.example.data.dao.VehicleDao;
import com.example.data.entity.Vehicle;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class VehicleDaoImpl implements VehicleDao {

    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Vehicle> findAll() {
        return em.createQuery("select v from Vehicle v")
                .getResultList();
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        em.persist(vehicle);
        return vehicle;
    }

    @Override
    public void delete(Long id) {
        em.createQuery("delete from Vehicle v where v.id=:id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public Vehicle findById(Long id) {
        return (Vehicle) em.createQuery("SELECT v FROM Vehicle v where v.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Vehicle update(Vehicle vehicle) {
        return (Vehicle) em.merge(vehicle);
    }

}
