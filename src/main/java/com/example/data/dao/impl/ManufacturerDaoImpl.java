package com.example.data.dao.impl;

import com.example.data.dao.ManufacturerDao;
import com.example.data.entity.Manufacturer;
import com.example.data.repository.ManufacturerRepository;
import com.example.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//@Repository
@Component
public class ManufacturerDaoImpl implements ManufacturerDao {

//    @PersistenceContext
//    private EntityManager em;

    private ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerDaoImpl(ManufacturerRepository manufacturerRepository) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Manufacturer findById(Long id) {
        return this.manufacturerRepository.findById(id).orElseThrow(()
                -> new NotFoundException(String.format("Manufacturer with id '%s' not found",id)));
    }

//    @Override
//    public Manufacturer findById(Long id) {
//        return (Manufacturer) em.createQuery("SELECT m FROM Manufacturer m where m.id = :id")
//                .setParameter("id", id)
//                .getSingleResult();
//    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        this.manufacturerRepository.save(manufacturer);
        return manufacturer;
    }

    @Override
    public List<Manufacturer> findAll() {
        return (ArrayList) manufacturerRepository.findAll();
    }

    @Override
    public Manufacturer findIdByCompanyName(String companyName) {
        return this.manufacturerRepository.findManufacturerByCompanyName(companyName).orElseThrow(()
                ->new NotFoundException(String.format("Cant find Manufcturer by Company name'%companyName'",companyName)));
    }

    //    @Override
//    public Manufacturer save(Manufacturer manufacturer) {
//        em.persist(manufacturer);
//        return manufacturer;
//    }
//
//    @Override
//    public List<Manufacturer> findAll() {
//        return em.createQuery("SELECT m from Manufacturer m")
//                .getResultList();
//    }
//
//    @Override
//     public Manufacturer findIdByCompanyName(String companyName) {
//        return (Manufacturer) em.createQuery("SELECT m FROM Manufacturer m where m.company_name = :companyName")
//                .setParameter("companyName", companyName)
//                .getSingleResult();
//    }
}