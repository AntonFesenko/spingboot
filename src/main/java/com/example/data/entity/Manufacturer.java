package com.example.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode(of = "id")
@ToString
@Builder
@Entity
@Table(name = "manufacturer")
public class Manufacturer {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
//        @GeneratedValue(generator="increment")
//        @GenericGenerator(name="increment", strategy = "increment")
        @Setter(AccessLevel.NONE)
        private Long id;

        @Column(name = "company_name" , unique = true)
        private String  companyName;

        @Column(name = "car_model_name" )
        private String carModelName;

        @Column(name = "address")
        private String address;

        @Column(name = "foundation_year")
        private Integer foundationYear;

        @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
        @JsonIgnore
        private Set<Vehicle> vehicles = new HashSet<Vehicle>();
}
