package com.example.web.controller;

import com.example.data.entity.Manufacturer;
import com.example.exception.ManufacturerValidationException;
import com.example.service.ManufacturerService;
import com.example.web.validation.ManufacturerValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manufacturers")
@Api(value = "Endpoint Manufacturers", description = "Manufacturer description")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Manufacturer>> getAll(){
        return new ResponseEntity<>(manufacturerService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "create Manufacturer")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse( code = 201, message = "create manufacturer"),
            @ApiResponse( code = 400, message = "validation error"),
            @ApiResponse( code = 404, message = "resource is not find"),
    })
    public ResponseEntity create(@RequestBody Manufacturer manufacturer) throws ManufacturerValidationException {
        ManufacturerValidator.validate(manufacturer);
        manufacturerService.save(manufacturer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/get/{company}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getManufacterer(@PathVariable("company") String company){
        Manufacturer manufacturer = manufacturerService.findIdByCompanyName(company);
        return new ResponseEntity<>(manufacturer, HttpStatus.OK);
    }

}
