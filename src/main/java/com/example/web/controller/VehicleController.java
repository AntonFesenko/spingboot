package com.example.web.controller;

import com.example.data.entity.Manufacturer;
import com.example.data.entity.Vehicle;
import com.example.service.ManufacturerService;
import com.example.service.VehicleService;
import com.example.web.dto.VehicleDto;
import com.example.web.validation.VehicleValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vehicles")
@Slf4j
public class VehicleController {

    private final VehicleService vehicleService;
    private final ManufacturerService manufacturerService;

    @Autowired
    public VehicleController(VehicleService vehicleService, ManufacturerService manufacturerService) {
        this.vehicleService = vehicleService;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getAllVehicles(){
        log.info("Request to return all vehicles");
        List<VehicleDto> vehicleDtos = vehicleService.getAllVehicles().stream()
                .map(VehicleDto::from)
                .collect(Collectors.toList());
        return new ResponseEntity<>(vehicleDtos, HttpStatus.OK);
    }

    @PostMapping(value = "/manufacturers/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@PathVariable(name = "id")  Long id, @RequestBody Vehicle vehicle){
        vehicleService.createVehicleForManufacturer(id, vehicle);
        VehicleValidator.validate(vehicle);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getById(@PathVariable("id") Long id){
        VehicleDto vehicleDto = VehicleDto.from(vehicleService.findById(id));
        return new ResponseEntity<>(vehicleDto, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        vehicleService.delete(id);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

    @PutMapping(value = "update/manufacturers/{manufacturersId}", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity update(@PathVariable(value = "manufacturersId")  Long manufacturersId,
                                 @RequestBody Vehicle vehicle){
        Vehicle vehicleUpdate = vehicleService.updateVehicle(manufacturersId, vehicle);
        VehicleValidator.validate(vehicle);
        return new ResponseEntity<>(vehicleUpdate, HttpStatus.OK);
    }
}
