package com.example.web.validation;

import com.example.data.entity.Vehicle;
import com.example.exception.VehicleValidationException;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VehicleValidator {

    private static final String EMPTY_PROPERTY_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must be provided";
    private static final String REGEX_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must match these parameters: '%s'";

    private static String VALID_CAPACITY_REGEX = "^[0-8]+(\\.[0-9]+)?$";
    private static String VALID_SEATS_REGEX = "^[1-7]$";
    private static String VALID_WHEELS_REGEX = "^[2-6]$";
    private static String VALID_MASS_REGEX = "^(([1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|10000)*[.])?([0-9])$";
    private static String VALID_VIN_REGEX = "^(([a-h,A-H,j-n,J-N,p-z,P-Z,0-9]{9})([a-h,A-H,j-n,J-N,p,P,r-t,R-T,v-z,V-Z,0-9])([a-h,A-H,j-n,J-N,p-z,P-Z,0-9])(\\d{6}))$";

    public static void validate(Vehicle vehicle) throws VehicleValidationException {
        validateNotEmptyProperty(vehicle.getColor(), "color");
        validateNotEmptyProperty(vehicle.getModel(), "model");

        validateWithRegularExpression(vehicle.getEngineCapacity(),VALID_CAPACITY_REGEX,"engineCapacity",
                "Capacity must consist of range 0-9.0-9");
        validateWithRegularExpression(vehicle.getSeats(),VALID_SEATS_REGEX,"seats",
                "Seats must consist of range 1-9");
        validateWithRegularExpression(vehicle.getWheels(),VALID_WHEELS_REGEX,"wheels",
                "Wheels must consist of range 2-6");
        validateWithRegularExpression(vehicle.getVehicleMass(),VALID_MASS_REGEX,"vehicleMass",
                "Mass must consist of range 100 - 10000");
        validateWithRegularExpression(vehicle.getVinNumber(),VALID_VIN_REGEX,"vinNumber",
                "Vin number must consist of 17 numbers");

    }

    private static void validateNotEmptyProperty(Object value, String propertyName) throws VehicleValidationException {
        if (value == null || StringUtils.isEmpty(value)) {
            throw new VehicleValidationException(String.format(EMPTY_PROPERTY_EXCEPTION_MESSAGE, propertyName));
        }
    }

    private static void validateWithRegularExpression(Object value, String regex, String propertyName, String exceptionMessage) throws VehicleValidationException {
        Matcher matcher = Pattern.compile(regex).matcher(String.valueOf(value));
        if (!matcher.matches()) {
            throw new VehicleValidationException(String.format(REGEX_EXCEPTION_MESSAGE, propertyName, exceptionMessage));
        }
    }
}
