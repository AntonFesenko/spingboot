package com.example.service.impl;

import com.example.data.dao.ManufacturerDao;
import com.example.data.entity.Manufacturer;
import com.example.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ManufacturereServiceImpl implements ManufacturerService {

    private final ManufacturerDao manufacturerDao;

    @Autowired
    public ManufacturereServiceImpl(ManufacturerDao manufacturerDao) {
        this.manufacturerDao = manufacturerDao;
    }

    @Override
    public Manufacturer findById(Long id) {
        return manufacturerDao.findById(id);
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        return manufacturerDao.save(manufacturer);
    }

    @Override
    public List<Manufacturer> findAll() {
        return manufacturerDao.findAll();
    }

    @Override
    public Manufacturer findIdByCompanyName(String companyName) {
        return manufacturerDao.findIdByCompanyName(companyName);
    }
}
