package com.example.service;

import com.example.data.entity.Vehicle;

import java.util.List;

public interface VehicleService {

    List<Vehicle> getAllVehicles();

    Vehicle save(Vehicle vehicle);

    void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle);

    void delete(Long id);

    Vehicle findById(Long id);

    void update(Long id, Vehicle vehicle);

    Vehicle updateVehicle(Long manufacturerId, Vehicle vehicle);
}