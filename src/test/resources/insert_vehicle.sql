DELETE FROM vehicle;

INSERT INTO vehicle (
id, color, engine_capacity, model, seats, mass, vin, wheels, manufacturer_id
) VALUES (
1, 'Red', 3.5, 'X8', 2, 8300.0, 'WP0ZZZ99ZTS392121', 6, 1);