DELETE FROM manufacturer;

INSERT INTO manufacturer (
id, address, car_model_name, company_name, foundation_year
) VALUES (1, 'Berlin', 'BMW', 'BMW', 1904);