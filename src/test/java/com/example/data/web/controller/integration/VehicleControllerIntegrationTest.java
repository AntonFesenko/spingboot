package com.example.data.web.controller.integration;


import com.example.data.dao.VehicleDao;
import com.example.data.entity.Vehicle;
import com.example.data.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class VehicleControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    VehicleDao vehicleDao;

    @Test
    void whenEmptyVehicleListShouldRespondOkTest() throws Exception {
        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    @SqlGroup({@Sql(scripts = {"/insert_manufacturer.sql"}), @Sql(scripts = {"/insert_vehicle.sql"})})
    @DisplayName("should return 'vehicle with id 1 and HTTP 200'")
    void whenListOfVehicleListShouldRespondOkAndReturnListTest() throws Exception {

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].color", is("Red")))
                .andExpect(jsonPath("$[0].engineCapacity", is(3.5)))
                .andExpect(jsonPath("$[0].model", is("X8")))
                .andExpect(jsonPath("$[0].seats", is(2)))
                .andExpect(jsonPath("$[0].vehicleMass", is(8300.0)))
                .andExpect(jsonPath("$[0].vinNumber", is("WP0ZZZ99ZTS392121")))
                .andExpect(jsonPath("$[0].wheels", is(6)))
                .andExpect(jsonPath("$[0].manufacturer", is("BMW")))
                .andDo(print());
    }

    @Test
    @Sql(scripts = {"/insert_manufacturer.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @DisplayName("should return 'HTTP 201'")
    void whenCreateVehicleShouldReturnCreatedTest() throws Exception {

        Vehicle expected = Vehicle.builder()
                .engineCapacity(3.5)
                .seats(2)
                .wheels(6)
                .vinNumber("WP0ZZZ99ZTS392121")
                .vehicleMass(8300.0)
                .color("Black")
                .model("X8")
                .build();

        mockMvc.perform(post("/vehicles/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andDo(print())
                .andExpect(status().isCreated());

        List<Vehicle> vehicles = vehicleDao.findAll();

        assertEquals(1, vehicles.size());
        Vehicle actual = vehicles.get(0);

        assertEquals(actual.getEngineCapacity(), expected.getEngineCapacity());
        assertEquals(actual.getSeats(), expected.getSeats());
        assertEquals(actual.getWheels(), expected.getWheels());
        assertEquals(actual.getVinNumber(), expected.getVinNumber());
        assertEquals(actual.getVehicleMass(), expected.getVehicleMass());
        assertEquals(actual.getColor(), expected.getColor());
        assertEquals(actual.getModel(), expected.getModel());
        assertNotNull(actual.getId());
    }

    @Test
    @SqlGroup({@Sql(scripts = {"/insert_manufacturer.sql"})})
    @DisplayName("should return 'updated vehicle and HTTP 200'")
    void whenUpdateVehicleShouldRespondOkTest() throws Exception {

        Vehicle expected = Vehicle.builder()
                .id(1L)
                .engineCapacity(4.5)
                .seats(4)
                .wheels(6)
                .vinNumber("WP0ZZZ99ZTS392121")
                .vehicleMass(9300.0)
                .color("White")
                .model("X9")
                .build();

        mockMvc.perform(put("/vehicles/update/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isOk());

        List<Vehicle> vehicles = vehicleDao.findAll();

        assertEquals(1, vehicles.size());
        Vehicle actual = vehicles.get(0);

        assertEquals(actual.getEngineCapacity(), expected.getEngineCapacity());
        assertEquals(actual.getSeats(), expected.getSeats());
        assertEquals(actual.getWheels(), expected.getWheels());
        assertEquals(actual.getVinNumber(), expected.getVinNumber());
        assertEquals(actual.getVehicleMass(), expected.getVehicleMass());
        assertEquals(actual.getColor(), expected.getColor());
        assertEquals(actual.getModel(), expected.getModel());
        assertNotNull(actual.getId());
    }

    @Test
    @SqlGroup({@Sql(scripts = {"/insert_manufacturer.sql"}), @Sql(scripts = {"/insert_vehicle.sql"})})
    @DisplayName("when delete should return empty list and HTTP 200")
    void whenDeleteVehicleShouldRespondOkTest() throws Exception {

        mockMvc.perform(delete("/vehicles/delete/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk());

        List<Vehicle> vehicles = vehicleDao.findAll();

        assertEquals(0, vehicles.size());
    }
}
