package com.example.data.web.controller;

import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class NotExistEndpointTest extends ControllerBaseTest {

    @Test
    void whenGetNonExistEndpointShouldRespond404Test() throws Exception {

        mockMvc.perform(get("/not_exist_endpoint"))
                .andExpect(status().is(404));

    }
}
